package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/go-redis/redis"
	"github.com/jackc/pgx/v5"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

func main() {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	viper.AddConfigPath(".")
	viper.SetConfigFile(".env")

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Msgf("%s", err)
	}

	ctx := context.Background()

	//DB connec
	conn, err := pgx.Connect(ctx, viper.GetString("DATABASE_URL"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	//DB request
	//pgselect(*conn, "select primarytitle FROM titlebasics where tconst IN (select tconst from titleprincipals where nconst = (select nconst from namebasics where primaryname = 'Brad Pitt'))")

	pgselect(ctx, *conn, "select category from titleprincipals group by category")

}

func pgselect(ctx context.Context, pgconn pgx.Conn, query string) {

	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	cachedCategories, err := redisClient.Get("categories").Bytes()
	if err != nil {
		// Cache miss, we nedd to call the DB
		rows, err := pgconn.Query(ctx, query)
		if err != nil {
			fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
			os.Exit(1)
		}
		var categories []string
		for rows.Next() {
			values, err := rows.Values()

			if err != nil {
				log.Fatal().Msgf("error while iterating query result")
			} else {
				fmt.Printf("%s\n", values[0])
				categories = append(categories, string(values[0]))
			}
		}
		cachedCategories, err = json.Marshal(categories)
		err = redisClient.Set("categories", cachedCategories, 10*time.Second).Err()
		if err != nil {
			log.Fatal().Msgf("%s", err)
		}
	} else {
		fmt.Printf("Categories : %s\n", cachedCategories)
	}
}
