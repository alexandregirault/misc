#!/bin/bash

mkdir -p datasets
DATASETS_DIR="datasets"
PGPASSWORD="testpasswd"

# Downloading datasets
for file in name.basics.tsv.gz title.akas.tsv.gz title.basics.tsv.gz title.crew.tsv.gz title.episode.tsv.gz title.principals.tsv.gz title.ratings.tsv.gz
do 
    if [ ! -f ${DATASETS_DIR}/${file} ]
    then
        wget -P ${DATASETS_DIR} https://datasets.imdbws.com/${file}
    else 
        echo "file ${file} already exist"
    fi
done

# Start Database
docker-compose up -d 

# create schema and load datasets
docker run -it --rm --volume "$(pwd):/opt/" -e PGPASSWORD=${PGPASSWORD} --network postgresql_back postgres:14.6-bullseye psql -h postgresql -U testuser -f /opt/load.sql
