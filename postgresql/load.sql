 \pset pager 0
drop table if exists titlebasic;
drop table if exists titlebasics;
CREATE TABLE titlebasics (
  tconst VARCHAR(10),
  titleType VARCHAR(50),
  primaryTitle VARCHAR,
  originalTitle VARCHAR,
  isAdult BOOL,
  startYear int,
  endYear int,
  runtimeMinutes int,
  genres VARCHAR
); 
\copy titlebasics from program 'zcat opt/datasets/title.basics.tsv.gz|tail -n +2' with (delimiter E'\t', null '\N')

drop table if exists titleratings;
CREATE TABLE titleratings(
  tconst VARCHAR(10),
  averageRating float,
  numVotes int
);
\copy titleratings from program 'zcat opt/datasets/title.ratings.tsv.gz|tail -n +2' with (delimiter E'\t', null '\N')


drop table if exists titleprincipals;
CREATE TABLE titleprincipals(
  tconst VARCHAR(10),
  ordering int,
  nconst VARCHAR(10),
  category VARCHAR(20),
  job  VARCHAR,
  characters VARCHAR
);
\copy titleprincipals from program 'zcat opt/datasets/title.principals.tsv.gz|tail -n +2' with (delimiter E'\t', null '\n')


drop table if exists titleepisode;
CREATE TABLE titleepisode(
  tconst VARCHAR(10),
  parentTconst VARCHAR(10),
  seasonNumber int,
  episodeNumber int
);
\copy titleepisode from program 'zcat opt/datasets/title.episode.tsv.gz|tail -n +2' with (delimiter E'\t', null '\N')


drop table if exists titlecrew;
CREATE TABLE titlecrew(
  tconst VARCHAR(10),
  directors VARCHAR,
  writers VARCHAR
);
\copy titlecrew from program 'zcat opt/datasets/title.crew.tsv.gz|tail -n +2' with (delimiter E'\t', null '\N')


drop table if exists titleakas;
CREATE TABLE titleakas(
  titleId VARCHAR(10),
  ordering int,
  title VARCHAR,
  region VARCHAR(5),
  language VARCHAR(5),
  types VARCHAR(20),
  attributes VARCHAR,
  isOriginalTitle bool
);
\copy titleakas from program 'zcat opt/datasets/title.akas.tsv.gz|tail -n +2' with (delimiter E'\t', null '\N')


drop table if exists namebasics;
CREATE TABLE namebasics(
  nconst VARCHAR(10),
  primaryName VARCHAR,
  birthYear int,
  deathYear int,
  primaryProfession VARCHAR,
  knownForTitles VARCHAR
);
\copy namebasics from program 'zcat opt/datasets/name.basics.tsv.gz|tail -n +2' with (delimiter E'\t', null '\N')

