data "aws_caller_identity" "compteperso" {
  provider = aws.fr
}

data "aws_ami" "ubuntu_22_04_x86_fr" {
  provider    = aws.fr
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_ami" "ubuntu_22_04_arm_fr" {
  provider    = aws.fr
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-arm64-server-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_ami" "docker-base" {
  provider    = aws.fr
  most_recent = true
  owners      = ["767410294669"]

  filter {
    name   = "name"
    values = ["docker-base-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_vpc" "default_fr" {
  provider = aws.fr
  default  = true
}

data "aws_subnets" "default_fr" {
  provider = aws.fr
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default_fr.id]
  }
}

data "aws_vpc" "main_fr" {
  provider = aws.fr
    filter {
      name   = "tag:Name"
      values = ["main_fr"]
  }
}

data "aws_subnets" "main_private_01_fr" {
  provider = aws.fr
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main_fr.id]
  }
  filter{
    name   = "tag:Type"
    values = ["private_01"]
  }
}

data "aws_subnets" "main_public_01_fr" {
  provider = aws.fr
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main_fr.id]
  }
  filter{
    name   = "tag:Type"
    values = ["public_01"]
  }
}