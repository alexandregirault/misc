module "bastion" {
  source       = "../tf_modules/autoscaling_stack"
  service_name = "bastion"
  image_id     = "ami-090fdf6ee6fc5d597"
  ssh_key_name = "default Lab AWS key"
  vpc_id       = data.aws_vpc.main_fr.id
  subnets      = data.aws_subnets.main_public_01_fr.ids
  lt_instance_types = ["t4g.small"]
  desired_capacity = 0
  min_size = 0
  iam_instance_profile = aws_iam_instance_profile.bastion.name
}

module "bastion2" {
  source       = "../tf_modules/autoscaling_stack"
  service_name = "bastion2"
  image_id     = "ami-090fdf6ee6fc5d597"
  ssh_key_name = "default Lab AWS key"
  vpc_id       = data.aws_vpc.main_fr.id
  subnets      = data.aws_subnets.main_public_01_fr.ids
  lt_instance_types = ["t4g.small"]
  desired_capacity = 0
  min_size = 0
  iam_instance_profile = aws_iam_instance_profile.bastion.name
}

module "db" {
  source       = "../tf_modules/autoscaling_stack"
  service_name = "database"
  image_id     = data.aws_ami.docker-base.id
  ssh_key_name = "default Lab AWS key"
  vpc_id       = data.aws_vpc.main_fr.id
  subnets      = data.aws_subnets.main_private_01_fr.ids
  lt_instance_types = ["t4g.small"]
  desired_capacity = 0
  min_size = 0
}