
resource "aws_iam_instance_profile" "bastion" {
  name = "bastion_profile"
  role = aws_iam_role.bastion.name
}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "bastion" {
  name               = "bastion"
  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json

  inline_policy {
    name   = "get-ssh-keys"
    policy = jsonencode(
      {
        Version   = "2012-10-17"
        Statement = [
          {
            Sid    = ""
            Effect = "Allow"
            Action = [
              "iam:GetSSHPublicKey",
              "iam:ListSSHPublicKeys"
            ]
            Resource = "arn:aws:iam::${data.aws_caller_identity.compteperso.account_id}:user/*"
          }
        ]
      }
    )
  }

  inline_policy {
    name   = "list-group-user"
    policy = jsonencode(
      {
        Version   = "2012-10-17"
        Statement = [
          {
            Sid    = ""
            Effect = "Allow"
            Action = ["iam:ListGroupsForUser",
                      "iam:GetGroup"
            ]
            Resource = ["arn:aws:iam::${data.aws_caller_identity.compteperso.account_id}:user/*",
                        "arn:aws:iam::${data.aws_caller_identity.compteperso.account_id}:group/*"
            ]
          },
          {
            Sid    = ""
            Effect = "Allow"
            Action = "iam:List*"
            Resource = "*" 
          }
        ]
      }
    )
  }
}