provider "aws" {
  region = "eu-west-3"
}

provider "aws" {
  alias  = "ir"
  region = "eu-west-1"
}

provider "aws" {
  alias  = "fr"
  region = "eu-west-3"
}

terraform {
  backend "s3" {
    bucket         = "tf-states-labawsalex"
    key            = "global/s3/lab-sonarqube.state"
    region         = "eu-west-3"
    dynamodb_table = "terraforms_locks"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.1"
    }
  }
  required_version = "1.4.6"
}
