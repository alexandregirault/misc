resource "aws_launch_template" "lt" {
  name_prefix   = var.service_name
  image_id      = var.image_id
  key_name      = var.ssh_key_name
  user_data     = var.user_data

  instance_requirements {
    allowed_instance_types = var.lt_instance_types
    memory_mib {
      min = 1023
    }
    vcpu_count {
      min = 1
    }

    burstable_performance = "included"
  }

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  vpc_security_group_ids = [aws_security_group.base-sg.id]
}

resource "aws_autoscaling_group" "asg" {
  name                      = var.service_name
  desired_capacity          = var.desired_capacity
  min_size                  = var.min_size
  max_size                  = var.max_size
  health_check_grace_period = var.health_check_grace_period
  vpc_zone_identifier       = var.subnets
  protect_from_scale_in     = var.protect_from_scale_in

  mixed_instances_policy {
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.lt.id
        version = "$Latest"
      }
    }
  }


  lifecycle {
    create_before_destroy = true
  }
  
  tag {
    key                 = "Name"
    value               = var.service_name
    propagate_at_launch = true
  }
}


resource "aws_security_group" "base-sg" {
  name        = "base-sg-${var.service_name}"
  description = "allow internal ssh"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

