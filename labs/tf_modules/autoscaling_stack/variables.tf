# Mandatory settings

variable "service_name" {
  description = "meaningfull and unique name for this autoscaling group (ex: bastion, grafana,...)"
  type        = string
}

variable "image_id" {
  description = "AMI id (ex: ami-05b5a865c3579bbc4)"
  type        = string
}

variable "ssh_key_name" {
  description = "EC2 SSH key copied at first boot"
  type        = string
}

variable "vpc_id" {
  description = "A VPC Id"
  type        = string
}

variable "subnets" {
  description = "A list of subnet IDs"
  type        = list(string)
  default     = null
}


# Optionnal settings

variable "lt_instance_types" {
  description = "EC2 default instance type, if no asg_instance_types specified"
  type        = list
  default     = ["t4g.small"]
}

variable "has_public_ip" {
  description = "has as public ip or not"
  type        = bool
  default     = false
}

variable "desired_capacity" {
  description = "desired default number of ec2 instances"
  type        = number
  default     = 1
}

variable "min_size" {
  description = "min number of ec2 instances"
  type        = number
  default     = 1
}

variable "max_size" {
  description = "max number of ec2 instances"
  type        = number
  default     = 1
}

variable "health_check_grace_period" {
  description = "default 5 min before health check "
  type        = number
  default     = 300
}

variable "protect_from_scale_in" {
  description = "activate if instance should be protected during scale in"
  type        = bool
  default     = false
}

variable "user_data" {
  description = "The Base64-encoded user data (cloud-init)"
  type        = string
  default     = null
}

variable "iam_instance_profile" {
  description = "optional iam_instance_profile"
  type        = string
  default     = null
}
