
# minimal
module "bastion" {
  source       = "../tf_modules/autoscaling_stack"
  service_name = "myService"
  image_id     = data.aws_ami.ubuntu_22_04_x86_fr.id
  ssh_key_name = "An EC2 ssh key name"
  vpc_id       = data.aws_vpc.default_fr.id
  subnets      = data.aws_subnets.default_fr.ids
}

