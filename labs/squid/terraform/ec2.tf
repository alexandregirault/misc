
resource "aws_launch_template" "squidcache" {
  provider = aws.ir
  name     = "squidcache"

  disable_api_stop        = false
  disable_api_termination = false

  image_id = data.aws_ami.ubuntu_22_04_ir.id
  instance_type = "t3a.nano"

  key_name = "default Lab AWS key"

  vpc_security_group_ids = [ aws_security_group.squidcache.id ]

  tag_specifications {
    resource_type = "instance"

    tags = {
      servertype = "squidcache"
    }
  }
  #user_data = filebase64("userdata.sh")
}

resource "aws_security_group" "squidcache" {
  provider    = aws.ir
  name        = "squidcache"
  description = "Allow inbound traffic"
  vpc_id      = data.aws_vpc.default_ir.id

  ingress {
    description      = "SSH from outside"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "allow from LAN"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["172.31.0.0/16"]
  }

  ingress {
    description      = "allow from personnal IP"
    from_port        = 3128
    to_port          = 3128
    protocol         = "tcp"
    cidr_blocks      = ["109.221.241.158/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}



resource "aws_autoscaling_group" "squidcache" {
  provider            = aws.ir
  capacity_rebalance  = true
  desired_capacity    = 1
  max_size            = 1
  min_size            = 1
  vpc_zone_identifier = data.aws_subnets.default_ir.ids

  mixed_instances_policy {
    instances_distribution {
      on_demand_percentage_above_base_capacity = 0
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.squidcache.id
        version            = "$Latest"
      }

      override {
        instance_type     = "t3a.small"
        weighted_capacity = "2"
      }

      override {
        instance_type     = "t3.small"
        weighted_capacity = "2"
      }

      override {
        instance_type     = "t2.small"
        weighted_capacity = "1"
      }

      override {
        instance_type     = "t3a.medium"
        weighted_capacity = "1"
      }
    }
  }
}