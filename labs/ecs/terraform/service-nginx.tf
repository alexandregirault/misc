resource "aws_lb_target_group" "ecs-sandbox-nginx" {
  count = var.ecs_sandbox_alb_enabled ? 1 : 0
  name        = "ecs-sandbox-nginx"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.default.id
}

resource "aws_ecs_task_definition" "nginx" {
  count = var.ecs_sandbox_enabled ? 1 : 0
  family = "webapp"
  requires_compatibilities = ["EC2"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  task_role_arn            = aws_iam_role.ecsTaskRole_exec.arn
  container_definitions = jsonencode([
    {
      name      = "nginx"
      image     = "public.ecr.aws/nginx/nginx:1.24"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
    }
  ])
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
}


resource "aws_ecs_service" "nginx" {
  count = var.ecs_sandbox_services_enabled ? 1 : 0
  name            = "nginx"
  cluster         = aws_ecs_cluster.sandbox[0].id
  task_definition = aws_ecs_task_definition.nginx[0].arn
  desired_count   = 1
  enable_execute_command   = true
  launch_type     = "EC2"

  network_configuration {
    subnets          = data.aws_subnets.default.ids
    security_groups  = [aws_security_group.allow_http[0].id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs-sandbox-nginx[0].arn
    container_name   = "nginx"
    container_port   = 80
  }

  lifecycle {
    ignore_changes = [desired_count,capacity_provider_strategy]
  }
}