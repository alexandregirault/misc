variable "ecs_sandbox_enabled"{
  type    = bool
  default = true
}

variable "ecs_sandbox_alb_enabled"{
  type    = bool
  default = true
}

variable "ecs_sandbox_services_enabled"{
  type    = bool
  default = true
}