resource "aws_route53_record" "ecs-sandbox" {
  count = var.ecs_sandbox_alb_enabled ? 1 : 0
  zone_id = data.aws_route53_zone.alexcloudlab.zone_id
  name    = "ecs-sandbox"
  type    = "A"

  alias {
    name                   = aws_lb.sandbox[0].dns_name
    zone_id                = aws_lb.sandbox[0].zone_id
    evaluate_target_health = false
  }
}

resource "aws_lb" "sandbox" {
  count = var.ecs_sandbox_alb_enabled ? 1 : 0
  name               = "ecs-sandbox-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_https[0].id]
  subnets            = data.aws_subnets.default.ids
  enable_deletion_protection = false

  tags = {
    Environment = "sandbox"
  }
}

resource "aws_lb_listener" "sandbox_https" {
  count = var.ecs_sandbox_alb_enabled ? 1 : 0
  load_balancer_arn = aws_lb.sandbox[0].arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS13-1-2-2021-06"
  certificate_arn   = data.aws_acm_certificate.ecs-sandbox.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs-sandbox-nginx[0].arn
  }
}


resource "aws_security_group" "allow_https" {
  count = var.ecs_sandbox_enabled ? 1 : 0
  name        = "allow_https"
  description = "Allow https inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "https from outside"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_https"
  }
}