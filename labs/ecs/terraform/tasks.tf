



resource "aws_security_group" "allow_http" {
  count = var.ecs_sandbox_enabled ? 1 : 0
  name        = "allow_http"
  description = "Allow http inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "http from outside"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    #cidr_blocks      = [for s in data.aws_subnet.default_cidrs : s.cidr_block]
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_http"
  }
}


resource "aws_iam_role" "ecsTaskRole_exec" {
  name               = "ecsTaskRole_exec"
  assume_role_policy = jsonencode({
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Principal":{
            "Service":[
               "ecs-tasks.amazonaws.com"
            ]
         },
         "Action":"sts:AssumeRole",
         "Condition":{
            "ArnLike":{
            "aws:SourceArn":"arn:aws:ecs:eu-west-3:767410294669:*"
            },
            "StringEquals":{
               "aws:SourceAccount":"767410294669"
            }
         }
      }
   ]
})

  inline_policy {
    name = "my_inline_policy"

    policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
       {
       "Effect": "Allow",
       "Action": [
            "ssmmessages:CreateControlChannel",
            "ssmmessages:CreateDataChannel",
            "ssmmessages:OpenControlChannel",
            "ssmmessages:OpenDataChannel"
       ],
      "Resource": "*"
      }
    ]
    })
  }
}