resource "aws_ecs_cluster" "sandbox" {
  count = var.ecs_sandbox_enabled ? 1 : 0
  name  = "sandbox"
}


resource "aws_ecs_cluster_capacity_providers" "sandbox" {
  count = var.ecs_sandbox_enabled ? 1 : 0
  cluster_name = aws_ecs_cluster.sandbox[0].name

  capacity_providers = ["FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 0
    weight            = 100
    capacity_provider = "FARGATE_SPOT"
  }
}

resource "aws_launch_configuration" "lc" {
    name_prefix    = "lc_ecs_sandbox"
    image_id = data.aws_ami.amazon_linux.id
    instance_type = "t3.medium"

    lifecycle {
      create_before_destroy = true
    }
    iam_instance_profile = aws_iam_instance_profile.ecs_service_role.name
    key_name             = "default Lab AWS key"
    security_groups      = [aws_security_group.ec2-sg.id]
    associate_public_ip_address = true
    user_data     = <<EOF
#!/bin/bash
sudo echo "ECS_CLUSTER=sandbox" >> /etc/ecs/ecs.config
ECS_CONTAINER_INSTANCE_PROPAGATE_TAGS_FROM=ec2_instance
EOF
}

resource "aws_autoscaling_group" "asg" {
    name =  "sandbox_asg"
    launch_configuration = aws_launch_configuration.lc.name
    desired_capacity = 1
    min_size = 0
    max_size = 2
    health_check_grace_period = 300
    vpc_zone_identifier = data.aws_subnets.default.ids
    protect_from_scale_in = false

    lifecycle {
        create_before_destroy = true
        ignore_changes = [
          tag,
        ]
    }
}

resource "aws_ecs_capacity_provider" "ecs_sandbox_ec2" {
  name = "sandbox_ec2"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.asg.arn
    managed_termination_protection = "DISABLED"

    managed_scaling {
      maximum_scaling_step_size = 1
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 2
    }
  }
}

resource "aws_security_group" "ec2-sg" {
    name = "allow-all-sg"
    description = "allow all"
    vpc_id = data.aws_vpc.default.id

    ingress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}


 

resource "aws_iam_role" "ecs-instance-role" {
    name = "ecs-sandbox-instance-role"

      assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_attachment" {
    role = aws_iam_role.ecs-instance-role.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_service_role" {
    role = aws_iam_role.ecs-instance-role.name
}