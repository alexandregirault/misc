#!/bin/bash

docker run \
    -p 9090:9090 \
    -v /home/alex/Documents/prometheus:/etc/prometheus \
    prom/prometheus:v2.33.4
