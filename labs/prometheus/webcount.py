from prometheus_client import Counter, start_http_server
import http.server 

REQUESTS = Counter('hello_total','Counter for requests on helloworld API')

class Myhandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        REQUESTS.inc()
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Hello world")

if __name__ == "__main__":
    start_http_server(8000)
    server = http.server.HTTPServer(('localhost',8080), Myhandler)
    server.serve_forever()