
variable "ami_date" { default = "" }

packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.0"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name            = "bastion-${var.ami_date}"
  spot_instance_types = ["t4g.xlarge", "c6g.xlarge", "r6g.medium", "m6g.large", "m7g.large", "r7g.medium"]
  spot_price          = "0.10"
  fleet_tags = {
    builder = "ami-factory"
  }
  ssh_interface = "public_ip"
  region        = "eu-west-3"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-arm64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  run_tags = {
    Name          = "bastion-${var.ubuntu_version}"
    OS_Version    = "Ubuntu"
    Base_AMI_Name = "{{ .SourceAMIName }}"
    Extra         = "{{ .SourceAMITags.TagName }}"
  }
  ssh_username = "ubuntu"
}

build {
  name = "buildBastion"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "shell" {
    inline = [
      "sudo mkdir -p /opt/sshd_scripts",
      "sudo mkdir -p /home/users",
      "sudo chown ubuntu:root /opt/sshd_scripts",
      "sudo chmod 777 /home/users"
    ]
  }

  provisioner "file" {
    destination = "/opt/sshd_scripts/authorized_keys_iam.sh"
    source      = "./files/authorized_keys_iam.sh"
  }

  provisioner "file" {
    destination = "/opt/sshd_scripts/import_iam_users.sh"
    source      = "./files/import_iam_users.sh"
  }

  provisioner "file" {
    destination = "/opt/sshd_scripts/sshd_config"
    source      = "./files/sshd_config"
  }

  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive"
    ]
    inline = [
      "sleep 30", 
      "sudo apt-get update",
      "sudo apt-get remove --purge -y ec2-instance-connect",
      "sudo apt-get install -y awscli",
      "sudo chown -R root:root /opt/sshd_scripts/",
      "sudo chmod -R 755 /opt/sshd_scripts/",
      "sudo chmod +x /opt/sshd_scripts/import_iam_users.sh",
      "sudo cp /opt/sshd_scripts/sshd_config /etc/ssh/sshd_config",
      "echo '*/5 * * * * root /opt/sshd_scripts/import_iam_users.sh' |sudo tee /etc/cron.d/import_iam_users"
    ]
  }
}
