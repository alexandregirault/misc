#!/bin/sh
set -ex

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

aws iam list-users --path / --query "Users[].[UserName]" --output text | while read User; do
  if id -u "$User" >/dev/null 2>&1; then
    echo "$User exists"
  else
    if [ $( aws iam list-user-tags --user-name ${User} --query "Tags[].[Key=='allowBastion']" --output=text ) = 'True' ]
    then
      /usr/sbin/adduser --home /home/users --disabled-password --gecos "" "$User"
      echo "$User ALL=(ALL) NOPASSWD:ALL" > "/etc/sudoers.d/$User"
    fi
  fi
done
