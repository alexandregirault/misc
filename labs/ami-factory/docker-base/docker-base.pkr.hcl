variable "ubuntu_version" { default = "22.04" }
variable "ami_date" { default = "" }

packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.0"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name            = "docker-base-${var.ami_date}"
  spot_instance_types = ["t4g.xlarge", "c6g.xlarge", "r6g.medium", "m6g.large", "m7g.large", "r7g.medium"]
  spot_price          = "0.10"
  fleet_tags = {
    builder = "ami-factory"
  }
  ssh_interface = "public_ip"
  region        = "eu-west-3"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/hvm-ssd/ubuntu-jammy-${var.ubuntu_version}-arm64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  run_tags = {
    Name          = "docker-base-${var.ubuntu_version}"
    OS_Version    = "Ubuntu"
    Base_AMI_Name = "{{ .SourceAMIName }}"
    Extra         = "{{ .SourceAMITags.TagName }}"
  }
  ssh_username = "ubuntu"
}

build {
  name = "buildDockerBase"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive"
    ]
    inline = [
      "sleep 30",
      "sudo apt-get update",
      "sudo apt-get install ca-certificates curl gnupg",
      "sudo install -m 0755 -d /etc/apt/keyrings",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg",
      "sudo chmod a+r /etc/apt/keyrings/docker.gpg",
      "echo \"deb [arch=arm64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu   jammy stable\" |sudo tee /etc/apt/sources.list.d/docker.list",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin",
      "sudo docker run hello-world"
    ]
  }
}
