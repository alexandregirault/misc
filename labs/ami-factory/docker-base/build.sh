#!/bin/bash
# Build a new AMI 


AMI_DATE=$( date +%Y%m%d-%H%M )

packer init .
packer fmt .
packer validate .
packer build -var "ubuntu_version=22.04" -var "ami_date="${AMI_DATE} docker-base.pkr.hcl
