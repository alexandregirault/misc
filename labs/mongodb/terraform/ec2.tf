
resource "aws_launch_template" "mongodb" {
  name = "mongodb"

  disable_api_stop        = false
  disable_api_termination = false

  image_id = data.aws_ami.ubuntu_20_04.id
  instance_type = "m6i.large"

  key_name = "default Lab AWS key"

  vpc_security_group_ids = [ aws_security_group.mongodb.id ]

  tag_specifications {
    resource_type = "instance"

    tags = {
      servertype = "mongodb"
    }
  }
}

resource "aws_security_group" "mongodb" {
  name        = "mongodb"
  description = "Allow inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "SSH from outside"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "mongo from LAN"
    from_port        = 27017
    to_port          = 27017
    protocol         = "tcp"
    cidr_blocks      = ["172.31.0.0/16"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}



resource "aws_autoscaling_group" "mongodb" {
  capacity_rebalance  = true
  desired_capacity    = 3
  max_size            = 3
  min_size            = 3
  vpc_zone_identifier = data.aws_subnets.default.ids

  mixed_instances_policy {
    instances_distribution {
      on_demand_percentage_above_base_capacity = 0
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.mongodb.id
        version            = "$Latest"
      }

      override {
        instance_type     = "t3a.large"
        weighted_capacity = "1"
      }

      override {
        instance_type     = "t3.large"
        weighted_capacity = "1"
      }

      override {
        instance_type     = "t2.large"
        weighted_capacity = "1"
      }
    }
  }
}