
cat install.sh 
curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

sudo apt update
sudo apt install mongodb-org
sudo systemctl start mongod.service
sudo systemctl enable mongod

mongo --eval 'db.runCommand({ connectionStatus: 1 })'



use testdb
db.user.insert({name: "Ada Lovelace", age: 20})

show dbs
show collections

172.31.20.165 mongo1
172.31.12.192 mongo2
172.31.34.208 mongo3


rs.initiate(
... {
... _id: "rs0",
... members: [
... { _id: 0, host: "mongo1" },
... { _id: 1, host: "mongo2" },
... { _id: 2, host: "mongo3" }
... ]
... })