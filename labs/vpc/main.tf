
resource "aws_vpc" "main" {
  cidr_block           = "172.16.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "main_fr"
  }
}

resource "aws_subnet" "main_private_01" {
  for_each          = var.main_vpc_subnets["main_private_01"]
  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value
  availability_zone = each.key

  tags = {
    Name = "main_private_01-${each.key}"
    Type = "private_01"
  }
}

resource "aws_subnet" "main_public_01" {
  for_each          = var.main_vpc_subnets["main_public_01"]
  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value
  availability_zone = each.key
  map_public_ip_on_launch = true

  tags = {
    Name = "main_public_01-${each.key}"
    Type = "public_01"
  }
}

resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "main-public" {
  vpc_id   = aws_vpc.main.id
  tags     = {"Name" = "route table for public subnets"}

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gw.id
  }
}

resource "aws_route_table_association" "main-public" {
  for_each       = aws_subnet.main_public_01
  subnet_id      = aws_subnet.main_public_01[each.key].id
  route_table_id = aws_route_table.main-public.id
}
