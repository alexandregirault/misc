

variable "main_vpc_subnets" {
  type = map(map(string))

  default = {
    "main_private_01" = {
      "eu-west-3a" = "172.16.0.0/24"
      "eu-west-3b" = "172.16.1.0/24"
      "eu-west-3c" = "172.16.2.0/24"
    }
    "main_public_01" = {
      "eu-west-3a" = "172.16.100.0/24"
      "eu-west-3b" = "172.16.101.0/24"
      "eu-west-3c" = "172.16.102.0/24"
    }
  }
}

variable "vpc_cidr" {
  type = map(string)
  default = {
    "main_private" = "172.16.0.0/16"
  }
}

