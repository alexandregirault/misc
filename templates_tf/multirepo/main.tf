module "aws-stock" {
  source       = "../mymodule"
  account_name = var.account_name
  env          = var.env

  db_backup_window      = var.db_backup_window
  db_maintenance_window = var.db_maintenance_window
  db_allocated_storage  = var.db_allocated_storage

}