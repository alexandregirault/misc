variable "account_name" {
  description = " AWS account name"
  type        = string
}
variable "env" {
  description = " AWS new environments"
  type        = string
}
variable "db_backup_window" {
  description = "The daily time range during which automated backups are created (UTC)"
  type        = string
  default     = "03:00-04:00"
}

variable "db_maintenance_window" {
  description = "The window to performance maintenance in"
  type        = string
  default     = "Mon:04:00-Mon:05:00"
}

variable "db_allocated_storage" {
  description = "The amount of allocated storage."
  type        = number
  default     = 10
}
