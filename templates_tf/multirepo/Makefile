VERSION=1.0.0
MODULE_NAME=$(shell basename $$(pwd))
VARS=vars/${env}.tfvars
S3_BUCKET=infra-terraform-${env}-myname-io
DYNAMODB_TABLE=${env}-infra-terraform-dynamodb_table
PLAN_FILE="${env}.tfplan"

all: help

apply: env 
	@cd terraform/ && \
	terraform apply \
		-lock=true \
		-input=false \
		${PLAN_FILE}

clean:
	@cd terraform/ && \
	rm -rf .terraform *.tfstate* *tfplan*

env: 
	@if [ -z ${env} ]; then \
		echo "Makefile: ENV was not set"; \
		exit 1; \
	 fi
	@if [ ! -f "terraform/$(VARS)" ]; then \
		echo "Makefile: Could not find variables file: $(VARS)"; \
		exit 1; \
	fi

fmt:
	@cd terraform/ && \
	terraform fmt \
		-check \

init: env
	@cd terraform/ && \
	terraform init \
		-input=false \
		-lock=true \
		-force-copy \
		-backend=true \
		-backend-config="bucket=${S3_BUCKET}" \
		-backend-config="key=${MODULE_NAME}/main/tfstate" \
		-backend-config="region=eu-west-1" \
		-backend-config="acl=private" \
		-backend-config="dynamodb_table=${DYNAMODB_TABLE}"

init-nobackend:
	@cd terraform/ && \
	terraform init \
		-input=false \
		-lock=false \
		-backend=false

plan: env
	@cd terraform/ && \
	terraform plan \
		-lock=true \
		-input=false \
		-refresh=true \
		-out=${PLAN_FILE} \
		-var-file="${VARS}"

plan-exit: env
	@cd terraform/ && \
	terraform plan \
		-lock=true \
		-input=false \
		-refresh=true \
		-detailed-exitcode \
		-var-file="${VARS}"

show: env
	@cd terraform/ && \
	terraform show -no-color ${PLAN_FILE} | tee -a plan.txt

validate:
	@cd terraform/ && \
	terraform validate

version:
	@terraform version

