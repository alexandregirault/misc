#- RDS Variables -----------------------------------------------------

variable "rds_mydb_config" {
  type = map(map(string))

  default = {
      "dev"     = {
        "enabled"               = true
        "instance_class"        = "db.t3.small"
        "allocation_storage"    = 20
        "max_allocated_storage" = 30
        "iops"                  = 3000
        "multi_az"              = false
      }
      "staging" = {
        "enabled"               = true
        "instance_class"        = "db.t3.small"
        "allocation_storage"    = 20
        "max_allocated_storage" = 40
        "iops"                  = 3000
        "multi_az"              = false
      }
      "prod"    = {
        "enabled"               = true
        "instance_class"        = "db.t3.small"
        "allocation_storage"    = 30
        "max_allocated_storage" = 40
        "iops"                  = 3000
        "multi_az"              = true
    }
  }
}

locals {
  rds_mydb_tags = {
                      "Project" = var.tag_project.mydb
                      "Team"    = var.tag_team.mydbs
                   }
}

#- RDS mydb -----------------------------------------------------

resource "aws_security_group" "rds_mydb" {
  count         = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name          = "rds-mydb"
  description   = "RDS mydb rules"
  vpc_id        = aws_vpc.env.id
  tags          = local.rds_mydb_tags

  ingress {
    description = "Allow DB Access from env"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr[terraform.workspace]]
  }
  
  ingress {
    description = "Allow DB Access from Shared"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr["shared"]]
  }

  ingress {
    description = "Allow DB Access from env legacy"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = var.vpc_legacy_cidr[terraform.workspace]
  }

  ingress {
    description     = "Allow db access from vpn"
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = ["${data.aws_caller_identity.shared.account_id}/sg-0c5b6b9c716edff10"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

module "rds_mydb" {
  count                        = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  source                       = "terraform-aws-modules/rds/aws"
  version                      = "5.2.0"

  identifier                   = "mydb"

  engine                       = "postgres"
  engine_version               = "14.5"
  family                       = "postgres14"
  instance_class               = var.rds_mydb_config[terraform.workspace].instance_class

  allocated_storage            = var.rds_mydb_config[terraform.workspace].allocation_storage
  storage_type                 = "gp3"
  iops                         = var.rds_mydb_config[terraform.workspace].iops

  db_name                      = "mydb"
  username                     = "rdsroot"
  port                         = 5432

  vpc_security_group_ids       = [aws_security_group.rds_mydb[0].id]
  max_allocated_storage        = var.rds_mydb_config[terraform.workspace].max_allocated_storage
  multi_az                     = var.rds_delivery_config[terraform.workspace].multi_az

  maintenance_window           = "Mon:03:00-Mon:04:00"
  backup_window                = "02:00-03:00"
  backup_retention_period      = 5
  apply_immediately            = true
  allow_major_version_upgrade  = false
  auto_minor_version_upgrade   = true
  performance_insights_enabled = true
  skip_final_snapshot          = true

  tags                         = local.rds_mydb_tags

  # DB subnet group
  create_db_subnet_group       = true
  subnet_ids                   = [for subnet in aws_subnet.env-private.*.id: subnet]
}

provider "postgresql" {
  alias           = "rds-mydb"
  host            = module.rds_mydb[0].db_instance_address
  port            = module.rds_mydb[0].db_instance_port
  username        = module.rds_mydb[0].db_instance_username
  password        = module.rds_mydb[0].db_instance_password
  database        = "mydb"
  sslmode         = "require"
  superuser       = false
  connect_timeout = 15
}

module "rds_mydb_accounts" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  source      = "../modules/db-accounts-postgresql"
  hostname    = module.rds_mydb[0].db_instance_address
  port        = module.rds_mydb[0].db_instance_port
  username    = module.rds_mydb[0].db_instance_username
  password    = module.rds_mydb[0].db_instance_password
  dbname      = "mydb"
  providers = {
    postgresql = postgresql.rds-mydb
  }
}

resource "aws_route53_record" "rds_mydb" {
  count    = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  zone_id  = aws_route53_zone.env_internal.zone_id
  name     = "mydb.rds"
  type     = "CNAME"
  ttl      = 300
  records  = [module.rds_mydb[0].db_instance_address]
}

resource "aws_ssm_parameter" "rds_mydb" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "/infra/${terraform.workspace}/mydb/rds/connection_string"
  description = "SSM mydb key"
  type        = "SecureString"
  value       = "postgres://${module.rds_mydb_accounts[0].accounts["application"].name}:${module.rds_mydb_accounts[0].accounts["application"].password}@${aws_route53_record.rds_mydb[0].fqdn}:${module.rds_mydb[0].db_instance_port}/mydb"
  tags        = local.rds_mydb_tags
}

resource "aws_ssm_parameter" "rds_mydb_username" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "/infra/${terraform.workspace}/mydb/rds/username"
  description = "SSM mydb Username"
  type        = "String"
  value       = module.rds_mydb_accounts[0].accounts["application"].name
  tags        = local.rds_mydb_tags
}
resource "aws_ssm_parameter" "rds_mydb_password" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "/infra/${terraform.workspace}/mydb/rds/password"
  description = "SSM mydb Password"
  type        = "SecureString"
  value       = module.rds_mydb_accounts[0].accounts["application"].password
  tags        = local.rds_mydb_tags
}
resource "aws_ssm_parameter" "rds_mydb_address" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "/infra/${terraform.workspace}/mydb/rds/address"
  description = "SSM mydb Address"
  type        = "String"
  value       = aws_route53_record.rds_mydb[0].fqdn
  tags        = local.rds_mydb_tags
}
resource "aws_ssm_parameter" "rds_mydb_port" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "/infra/${terraform.workspace}/mydb/rds/port"
  description = "SSM mydb Port"
  type        = "String"
  value       = module.rds_mydb[0].db_instance_port
  tags        = local.rds_mydb_tags
}
resource "aws_ssm_parameter" "rds_mydb_dbname" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "/infra/${terraform.workspace}/mydb/rds/dbname"
  description = "SSM mydb Dbname"
  type        = "String"
  value       = "mydb"
  tags        = local.rds_mydb_tags
}

resource "aws_ssm_parameter" "rds_mydb_ro" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "rds.ro.mydb"
  description = "RO SSM URI for service mydb"
  type        = "SecureString"
  key_id      = aws_kms_key.infra-secrets.id
  value       = "postgresql://${module.rds_mydb_accounts[0].accounts["readonly"].name}:${module.rds_mydb_accounts[0].accounts["readonly"].password}@${aws_route53_record.rds_mydb[0].fqdn}:${module.rds_mydb[0].db_instance_port}/mydb"
  tags        = local.rds_mydb_tags
}

resource "aws_ssm_parameter" "rds_mydb_rw" {
  count       = var.rds_mydb_config[terraform.workspace].enabled ? 1 : 0

  name        = "rds.rw.mydb"
  description = "RW SSM URI for service mydb"
  type        = "SecureString"
  key_id      = aws_kms_key.infra-secrets.id
  value       = "postgresql://${module.rds_mydb_accounts[0].accounts["application"].name}:${module.rds_mydb_accounts[0].accounts["application"].password}@${aws_route53_record.rds_mydb[0].fqdn}:${module.rds_mydb[0].db_instance_port}/mydb"
  tags        = local.rds_mydb_tags
}
