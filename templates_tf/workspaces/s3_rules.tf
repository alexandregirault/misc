#-- mybucket S3 Bucket -------------------------------
locals {
  s3_mybucket_tags = {
                    "Project" = var.tag_project.mybucket
                    "Team"    = var.tag_team.aftersales
                  }
}

resource "aws_s3_bucket" "mybucket" {
  bucket = "mybucket-storage-${terraform.workspace}-mydomain"
  tags     = local.s3_mybucket_tags
}

resource "aws_s3_bucket_acl" "mybucket" {
  bucket   = aws_s3_bucket.mybucket.id
  acl      = "private"
}

resource "aws_s3_bucket_public_access_block" "mybucket" {
  bucket                  = aws_s3_bucket.mybucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_ssm_parameter" "mybucket_bucket_name_parameter" {
  name  = "/mybucket/${terraform.workspace}/s3/storage-bucket/name"
  type  = "String"
  value = "${aws_s3_bucket.mybucket.id}"
}

resource "aws_ssm_parameter" "mybucket_bucket_arn_parameter" {
  name  = "/mybucket/${terraform.workspace}/s3/storage-bucket/arn"
  type  = "String"
  value = "${aws_s3_bucket.mybucket.arn}"
}