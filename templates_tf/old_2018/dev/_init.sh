#!/usr/bin/env bash
set -e

TF_VERSION=$(cat .terraform-version)

install(){
  echo "==> init.sh: Check required terraform version (${TF_VERSION})"
  if tfenv list | grep -q $TF_VERSION
  then
    echo "==> init.sh: Terraform ${TF_VERSION} already installed, skipping..."
    tfenv use ${TF_VERSION}
  else
    echo "==> init.sh: Terraform ${TF_VERSION} not installed, installing...."
    tfenv install
  fi
}

init(){
  terraform init -upgrade\
    -backend-config=.terraform-config \
    -lock=true \
    ../terraform/
}

install
init
