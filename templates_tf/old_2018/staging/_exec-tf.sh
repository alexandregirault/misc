#!/bin/bash

if [[ -z $1 ]]; then
  (echo "Usage: $0 [plan|apply|destroy]" && exit 1)
fi

terraform get -update ../terraform/

case "$1" in
  apply)
    terraform $1 -var-file=terraform.tfvars ../terraform/
    ;;
  *) 
    terraform $1 -var-file=terraform.tfvars ../terraform/
    ;;
esac