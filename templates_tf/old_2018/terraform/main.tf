resource "aws_s3_bucket" "static" {
  bucket = "${var.service}-static-${var.env}-myname-net"
  acl    = "public-read"

  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
    "Sid":"PublicReadGetObject",
    "Effect":"Allow",
    "Principal": "*",
    "Action":[
      "s3:GetObject"
    ],
    "Resource":[
      "arn:aws:s3:::${var.service}-static-${var.env}-myname-net/*"
    ]
  }]
}
POLICY


  tags = {
    Name                = "${var.env}.${var.service}.static.s3_bucket"
    "account-name"  = "${var.env}"
    "service"       = "${var.service}"
    "role"          = "static"
    "business-unit" = "my project"
    "managed-by"    = "Terraform"
  }
}

