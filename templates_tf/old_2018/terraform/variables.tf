#Provider vars
variable "region"                           { default = "eu-west-1" }

#Tagging vars
variable "service"                          { default = "front" }
variable "env"                              {}
